package com.sda.hibernate.enums;

public enum TaskType {
    NORMAL, HIGH_PRIORITY, BLOCKER
}
